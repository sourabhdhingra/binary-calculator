'use strict'

window.onload =  function () {
    let resultArea;
    let clickedBtn;
    let buttons = document.getElementsByClassName("btns");
    resultArea = document.getElementById("res");
    resultArea.innerHTML="";
    for(let btn of buttons) {
        btn.onclick = function () {
            clickedBtn = btn;
            writeToArea();
        }
    }

    function evaluate(expression) {
        try {
            let binNums = expression.match(/[0-1]+/g);
            console.log(binNums);
            for(let num of binNums) {
                console.log(num, parseInt(num, 2));
                expression = expression.replace(num.toString(), parseInt(num, 2).toString());
                console.log(expression);
                // expression = eval(expression).toString(2);
            }
            return eval(expression).toString(2);
        }
        catch (e) {
            if(e.name ==='SyntaxError')
                return 'Invalid Input';
        }


    }

    function writeToArea() {
        if(resultArea.innerHTML==='Invalid Input') resultArea.innerHTML="";

        if(clickedBtn.innerHTML !=='C' && clickedBtn.innerHTML!=='=' ) {
            resultArea.innerHTML = resultArea.innerHTML + clickedBtn.innerHTML;
        }
        else if (clickedBtn.innerHTML==='C') {
            resultArea.innerHTML = "";
        }
        else if(clickedBtn.innerHTML==='=') {
            resultArea.innerHTML = evaluate(resultArea.innerHTML);
        }
        console.log(resultArea.innerHTML);
    }

}

